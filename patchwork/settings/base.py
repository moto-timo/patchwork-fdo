"""
Base settings for the Patchwork project. These are included in all
configurations (prod/dev/test).

Design based on:
    http://www.revsys.com/blog/2014/nov/21/recommended-django-project-layout/

Docs:
    https://docs.djangoproject.com/en/3.2/ref/settings/
"""
import os

ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        os.pardir, os.pardir)

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'patchwork',
    'rest_framework',
    'django_filters',
]

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

MIDDLEWARE = [
    'patchwork.middleware.AccessControlAllowOriginMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'patchwork.threadlocalrequest.ThreadLocalRequestMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',
]

# Globalization
TIME_ZONE = 'Australia/Canberra'
LANGUAGE_CODE = 'en-au'
USE_I18N = True

# Testing
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# URLs
ROOT_URLCONF = 'patchwork.urls'

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(ROOT_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'patchwork.context_processors.settings',
                'patchwork.context_processors.site',
                'patchwork.context_processors.bundle',
            ],
        },
    },
]

# Email
DEFAULT_FROM_EMAIL = 'Patchwork <patchwork@patchwork.example.com>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL

# Auth settings
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth
LOGIN_URL = 'auth_login'
LOGIN_REDIRECT_URL = 'user'

# Sites settings
# https://docs.djangoproject.com/en/3.2/ref/settings/#sites
SITE_ID = 1

# Static files settings
# https://docs.djangoproject.com/en/3.2/ref/settings/#static-files
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(ROOT_DIR, 'htdocs'),
]

# REST Framework
REST_FRAMEWORK = {}

# Celery Task Queue
BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
# This timezone is used for time of periodic tasks, defaults to UTC.
# CELERY_TIMEZONE = 'Australia/Canberra'

# Patchwork settings
DEFAULT_PATCHES_PER_PAGE = 100

# Number of days to wait for users to confirm account registration:
CONFIRMATION_VALIDITY_DAYS = 7

NOTIFICATION_DELAY_MINUTES = 10
NOTIFICATION_FROM_EMAIL = DEFAULT_FROM_EMAIL

# Set to True to enable the Patchwork XML-RPC interface
ENABLE_XMLRPC = False

# Set to True to enable redirections or URLs from previous versions
# of patchwork
COMPAT_REDIR = True

# Set to True to always generate https:// links instead of guessing
# the scheme based on current access. This is useful if SSL protocol
# is terminated upstream of the server (e.g. at the load balancer)
FORCE_HTTPS_LINKS = False
