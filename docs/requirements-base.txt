python-dateutil>2.0,<3.0
djangorestframework>=3.13,<3.14
drf-nested-routers
jsonfield
django-filter==22.1
celery>=5.2,<6.0
redis
Markdown>=3.4,<3.5
lxml
